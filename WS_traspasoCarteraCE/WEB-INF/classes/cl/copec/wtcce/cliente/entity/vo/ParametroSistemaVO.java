package cl.copec.wtcce.cliente.entity.vo;

public class ParametroSistemaVO {
	private String codigoSistema;
	private String codigoParametro;
	private String codigoElemento;
	private String descripcionElement;
	private String valorElemento;
	
	public String getCodigoSistema() {
		return codigoSistema;
	}
	public void setCodigoSistema(String codigoSistema) {
		this.codigoSistema = codigoSistema;
	}
	public String getCodigoParametro() {
		return codigoParametro;
	}
	public void setCodigoParametro(String codigoParametro) {
		this.codigoParametro = codigoParametro;
	}
	public String getCodigoElemento() {
		return codigoElemento;
	}
	public void setCodigoElemento(String codigoElemento) {
		this.codigoElemento = codigoElemento;
	}
	public String getDescripcionElement() {
		return descripcionElement;
	}
	public void setDescripcionElement(String descripcionElement) {
		this.descripcionElement = descripcionElement;
	}
	public String getValorElemento() {
		return valorElemento;
	}
	public void setValorElemento(String valorElemento) {
		this.valorElemento = valorElemento;
	}
}
