package cl.copec.wtcce.resource;

import java.io.File;

import org.apache.log4j.PropertyConfigurator;

public class Log4jLauncher {
	private static final String log4jPropertiesPath = "/WEB-INF/resources/log4j.properties";
	
	public static void config(String contextBasePath, String contextName) {		
		
		String filePath = contextBasePath + log4jPropertiesPath;
		File newFile = new File(filePath);
		if (!newFile.exists()) {
//			System.out.println("ERROR: Could not find Log4j configuration file: " + filePath);
		} else {
//			System.out.println("Loading Log4j configuration file: " + filePath);
			PropertyConfigurator.configure(filePath);
		}
	}
}

