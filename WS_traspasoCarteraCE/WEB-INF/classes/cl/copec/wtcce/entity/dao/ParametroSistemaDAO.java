package cl.copec.wtcce.entity.dao;

import java.sql.Connection;

import com.jdbc.utils.core.annotation.FieldName;
import com.jdbc.utils.core.annotation.TableName;
import com.jdbc.utils.core.annotation.TableNameAlias;
import com.jdbc.utils.core.wrappers.AbstractTableDAO;

import cl.copec.wtcce.cliente.entity.vo.ParametroSistemaVO;

public class ParametroSistemaDAO extends AbstractTableDAO {
	@TableName
	private static final String tableName = "GRPARAMSISTEMAS";	
	@TableNameAlias
	private static final String tableNameAlias = "par";	
	@FieldName(key = true)
	public static final String codigoSistema = "PS_CODSISTEMA";
	@FieldName(key = true)
	public static final String codigoParametro = "PS_CODPARAM";
	@FieldName
	public static final String codigoElemento = "PS_CODELEMENTO";
	@FieldName
	public static final String descripcionElemento = "PS_DESELEMENTO";
	@FieldName
	public static final String valorElemento = "PS_VALORELEMENTO";
	
	public ParametroSistemaDAO(Connection connection) {
		super(connection, ParametroSistemaVO.class, false);
	}
}
