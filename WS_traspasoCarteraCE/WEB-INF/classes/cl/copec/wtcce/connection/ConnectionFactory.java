package cl.copec.wtcce.connection;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.ConnectionPoolDataSource;
import javax.sql.DataSource;
import javax.sql.PooledConnection;

import org.apache.log4j.Logger;

public class ConnectionFactory {
	private static Logger logger = Logger.getLogger(ConnectionFactory.class);
	private static DataSource dataSource = null;
	private static final String JNDI = "jdbc/cupon"; 

	public static Connection getConnection() {
		Connection connection = null;

		try {
			if (null == dataSource) {
				initDatasource();
			}

			if (null != dataSource) {
				logger.info("Obteniendo Conexi�n");
				connection = dataSource.getConnection();
			}

		} catch (SQLException e) {
			logger.error("ERROR: ", e);
		}

		return connection;
	}

	public static Connection getConnectionPool() throws NamingException, SQLException {
		ConnectionPoolDataSource dataSource = (ConnectionPoolDataSource) new InitialContext().lookup(JNDI);
		PooledConnection pooledConnection = dataSource.getPooledConnection();
		return pooledConnection.getConnection();
	}
	
	public static DataSource getDataSource() {
		if (null == dataSource) {
			initDatasource();
		}

		return dataSource;
	}
	
	private static void initDatasource() {
		try {
			logger.info("Inicializando Datasource");
			InitialContext initialContext = new InitialContext();		
			dataSource = (DataSource) initialContext.lookup(JNDI);  

		} catch (NamingException e) {
			logger.error("ERROR: ", e);
		}
	}
		
	public static void closeConnection(Connection connection) {		
		try {
			if (connection != null) {
				connection.close();
				logger.info("Conexi�n cerrada correctamente");
			}

		} catch (Exception e) {
			logger.error("ERROR: ", e);
		}
	}
	
	public static void finalizeTransaction(Connection connection, boolean status) {
		if (connection != null) {
			if (status) {
				commit(connection);

			} else {
				rollback(connection);
			}
		}
	}
	
	private static void commit(Connection connection) {
		try {
			connection.commit();
			
		} catch (SQLException e) {
			logger.error("ERROR: ", e);
		}
	}
	
	private static void rollback(Connection connection) {
		try {
			connection.rollback();
			
		} catch (SQLException e) {
			logger.error("ERROR: ", e);
		}
	}
}
