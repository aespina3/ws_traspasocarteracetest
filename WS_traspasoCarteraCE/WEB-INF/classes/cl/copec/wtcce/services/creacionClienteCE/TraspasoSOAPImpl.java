package cl.copec.wtcce.services.creacionClienteCE;

import cl.copec.wtcce.services.creacionClienteCE.brs.TraspasoCarteraCEBRS;

@javax.jws.WebService (endpointInterface="cl.copec.wtcce.services.creacionClienteCE.TraspasoCarteraCEWSSInterface", targetNamespace="http://www.example.org/Traspaso/", serviceName="traspasoCarteraCE", portName="TraspasoSOAP")
public class TraspasoSOAPImpl{

    public ResponseType creacionClientes(Request request) {
    	ResponseType response = new TraspasoCarteraCEBRS().creacionCliente(request);
        return response;
    }

}