package cl.copec.wtcce.services.creacionClienteCE.mgr;
/**
 * @author aespina
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.Logger;

import cl.copec.wtcce.services.creacionClienteCE.ClienteType;
import cl.copec.wtcce.services.creacionClienteCE.UsuarioType;

public class TraspasoCarteraCEMGR {
	private static Logger logger = Logger.getLogger(TraspasoCarteraCEMGR.class);	
	private Connection connection;
	
	public TraspasoCarteraCEMGR(Connection connection) {
		this.connection = connection;
	}
	
	

	public String servicioValidaProperties() throws Exception {
		String servicio="";
		Properties p = new Properties();
		InputStream is = new FileInputStream( Thread.currentThread().getContextClassLoader().getResource("/wsvalidausuario.properties").getPath());
		p.load(is);
		is.close();
	//	servicio=obtieneURL();
		servicio = p.getProperty("serviciovalidausr");

		return servicio;
	}
	
	public String obtieneURL(){
		//Consulta para obtener url
		PreparedStatement ps=null;
		ResultSet rs=null;
		String url="";
		String query=" SELECT PS_DESELEMENTO "+
		             " FROM grparmsis WHERE PS_CODSISTEMA='TC' and "+      
				     " PS_CODPARAM='VAL' and PS_CODELEMENTO ='VALUSUARIOLOGIN'    ";

		try {
			ps=connection.prepareStatement(query);
			rs=ps.executeQuery();
			while(rs.next()){
				url = rs.getString(1).trim();
			}			
		} catch(Exception e){
			logger.error("ERROR al obtener fecha actual: "+e);
		} finally {			
			if(rs!=null){try{rs.close();}catch(SQLException e){}}
			if(ps!=null){try{ps.close();}catch(SQLException e){}}
		}
		return url;
		             
	}




	public ArrayList<ClienteType> creacionClientes(UsuarioType usuario) {
		PreparedStatement ps=null;
		ResultSet rs=null;
		String query="CALL TPWEB030Z(?,?)";
		ArrayList<ClienteType> result = new ArrayList<ClienteType>();
		try {			
			ps=connection.prepareCall(query);			
			ps.setString(1, usuario.getUsuario());
			ps.setString(2, usuario.getFecha());
			logger.debug("CALL TPWEB030Z('"+usuario.getUsuario()+"','"+usuario.getFecha()+"')");
			rs=ps.executeQuery();
			while(rs.next()){
				ClienteType resp = new ClienteType();
				resp.setNombreCliente(rs.getString("Nombre_Cliente"));
				resp.setRazonSocial(rs.getString("Razon_Social"));
				resp.setGiro(rs.getString("Giro"));
				resp.setRut(rs.getString("RUT"));
				resp.setDireccion(rs.getString("Direccion"));
				resp.setComuna(rs.getString("Comuna"));
				resp.setPais(rs.getString("Pais"));
				resp.setRegion(rs.getString("Region"));
				resp.setTelefono(rs.getString("Telefono"));
				resp.setTelefono(rs.getString("Telefono"));
				resp.setEmail(rs.getString("Email"));
				resp.setRamo(rs.getString("Ramo"));
				result.add(resp);
			}
		} catch(Exception e){
			logger.error("ERROR creacionClientes: "+e);
		} finally {
			if(rs!=null){try{rs.close();}catch(SQLException e){}}
			if(ps!=null){try{ps.close();}catch(SQLException e){}}
		}
		return result;
	}



}
