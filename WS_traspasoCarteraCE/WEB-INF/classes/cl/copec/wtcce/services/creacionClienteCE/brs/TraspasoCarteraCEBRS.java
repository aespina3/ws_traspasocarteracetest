package cl.copec.wtcce.services.creacionClienteCE.brs;

import org.apache.log4j.Logger;

import cl.copec.wtcce.connection.ConnectionFactory;
import cl.copec.wtcce.services.creacionClienteCE.Request;
import cl.copec.wtcce.services.creacionClienteCE.ResponseType;
import cl.copec.wtcce.services.creacionClienteCE.mgr.TraspasoCarteraCEMGR;

public class TraspasoCarteraCEBRS {    
	private static Logger log = Logger.getLogger(TraspasoCarteraCEBRS.class);    
	
	public ResponseType creacionCliente(Request request){
		ResponseType response = new ResponseType();
		try
		{ 
			TraspasoCarteraCEMGR mgr= new TraspasoCarteraCEMGR(ConnectionFactory.getConnection());
			response.getCliente().addAll(mgr.creacionClientes(request.getUsuario()));
		}catch(Exception e)
		{
			log.error(e.getMessage());
		}		
		return response;
	}

	
}
