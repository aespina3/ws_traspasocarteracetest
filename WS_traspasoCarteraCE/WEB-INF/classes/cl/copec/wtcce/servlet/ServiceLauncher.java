package cl.copec.wtcce.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import cl.copec.wtcce.resource.Log4jLauncher;

public class ServiceLauncher implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent event) {
		String contextBasePath = event.getServletContext().getRealPath("/");
		String contextName = event.getServletContext().getServletContextName();
		
		Log4jLauncher.config(contextBasePath, contextName);
				
//		ApplicationResource.setupResources(contextBasePath);
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
	}
}
